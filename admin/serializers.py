from rest_framework import serializers

import vendor.models as models
from admin.config import config


class InvoiceSerializer(serializers.ModelSerializer):
    total_count = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    email = serializers.EmailField(source='user.email', read_only=True)

    class Meta:
        model = models.Invoice
        fields = '__all__'

    @staticmethod
    def create_related(invoice, spider_logs):
        for spider_log in spider_logs:
            try:
                models.InvoiceSpiderLog.objects.create(invoice=invoice, spider_log=spider_log)
            except:
                continue

    @staticmethod
    def get_full_name(obj):
        return '{} {}'.format(obj.user.first_name, obj.user.last_name)

    @staticmethod
    def get_total_count(obj):
        return models.InvoiceSpiderLog.objects.filter(invoice=obj).count()

    def create(self, validated_data):
        spider_logs = models.SpiderLogs.objects.filter(
            created__range=[
                validated_data['start_date'].strftime('%Y-%m-%d'), 
                validated_data['end_date'].strftime('%Y-%m-%d')
            ]
        )
        if not spider_logs:
            raise serializers.ValidationError(
                'There are no downladed files from {} to {}'.format(
                    validated_data['start_date'].strftime('%Y-%m-%d'), 
                    validated_data['end_date'].strftime('%Y-%m-%d')
                )
            )
        validated_data['rate'] = float(config.get('rate'))

        validated_data['price_amount'] = float(config.get('rate')) * len(spider_logs)

        invoice = models.Invoice.objects.create(**validated_data)

        self.create_related(invoice, spider_logs)

        return invoice

    def update(self, instance, validated_data):
        start_date = validated_data['start_date']
        end_date = validated_data['end_date']

        if start_date == instance.start_date and end_date == instance.end_date:
            return super(InvoiceSerializer, self).update(instance, validated_data)

        # delete all asigned to this invoice
        models.InvoiceSpiderLog.objects.filter(invoice=instance).delete()

        spider_logs = models.SpiderLogs.objects.filter(
            created__range=[
                validated_data['start_date'].strftime('%Y-%m-%d'), 
                validated_data['end_date'].strftime('%Y-%m-%d')
            ]
        )

        if not spider_logs:
            raise serializers.ValidationError(
                'There are no downladed files from {} to {}'.format(
                    validated_data['start_date'].strftime('%Y-%m-%d'),
                    validated_data['end_date'].strftime('%Y-%m-%d')
                )
            )

        validated_data['rate'] = float(instance.rate)

        validated_data['price_amount'] = float(instance.rate) * len(spider_logs)

        invoice = super(InvoiceSerializer, self).update(instance, validated_data)

        self.create_related(invoice, spider_logs)

        return invoice

