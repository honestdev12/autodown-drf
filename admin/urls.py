from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter


from admin import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
# router.register(r'vendor_site', views.VendorSiteViewSet, base_name='vendor_site')
# router.register(r'(?P<vendorsite_name>[ \w]+)/spider_job', viewset=views.SpiderJobViewSet, base_name='spider_job')
# router.register(r'(?P<spider_job_id>\d*)/?spider_logs', views.SpiderLogsViewSet, base_name="spider_logs")
# router.register(r'schedule', views.ScheduleViewSet)
# router.register(r'dropbox', views.DropBoxViewSet, base_name='dropbox')
router.register(r'(?P<user_id>\d*)/?(?P<paid>\w*)/?invoice', views.InvoiceViewSet, base_name='invoice')
router.register(r'users', views.UserViewSet, base_name='user')
router.register(r'vendor_site', views.VendorSiteViewSet, base_name='user')

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^config', views.ConfigView.as_view()),
]
