from rest_framework import permissions, viewsets, status
from vendor.models import User, Invoice, VendorSite, InvoiceSpiderLog
from vendor.serializers import UserSerializer, VendorSiteSerializer
from rest_framework.views import APIView

from rest_framework.response import Response
from .config import config
from .serializers import InvoiceSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser, )


class ConfigView(APIView):
    permissions = (permissions.IsAdminUser, permissions.IsAuthenticated, )

    @staticmethod
    def get(request):
        return Response(config.get(), status=status.HTTP_200_OK)

    def post(self, request):
        config.set(config=request.data)
        return self.get(request)


class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = InvoiceSerializer
    permissions = (permissions.IsAuthenticated, permissions.IsAdminUser, )

    def get_queryset(self):
        filter_options = {
            'spiderlog_set__isnull': False
        }
        order = self.request.query_params.get('sort')
        if not order:
            order = 'created'

        user_id = self.kwargs.get('user_id')
        paid = self.kwargs.get('paid')

        if user_id:
            user = User.objects.get(id=user_id)
            filter_options['user'] = user
                
        if paid and paid in ['paid', 'unpaid']:
            filter_options['paid'] = paid == 'paid'
        
        return Invoice.objects.filter(**filter_options).distinct().order_by(order)

    def perform_destroy(self, instance):
        InvoiceSpiderLog.objects.filter(invoice=instance).delete()
        instance.delete()


class VendorSiteViewSet(viewsets.ModelViewSet):
    serializer_class = VendorSiteSerializer
    permission_classes = (permissions.IsAuthenticated, )
    queryset = VendorSite.objects.all()

