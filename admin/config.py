import json
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config_path = os.path.join(BASE_DIR, 'config.json')


class Config:

    config = {}

    def __init__(self):
        self.load()

    def get(self, param=None):
        if param:
            return self.config.get(param)
        else:
            return self.config

    def set(self, param=None, value=None, config=None):
        if param:
            self.config[param] = value
            self.save(self.config)

        if config:
            self.config = config
            self.save(config)

    def save(self, config):
        with open(config_path, 'w') as f:
            json.dump(config, f)
            f.close()

    def load(self):
        with open(config_path, 'r') as f:
            self.config = json.loads(f.read())
            f.close()

config = Config()

