from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import TemplateView

urlpatterns = [
    url(r'^api/v1/admin/', include('admin.urls')),
    url(r'^api/v1/', include('vendor.urls')),
    url(r'admin/', admin.site.urls),
    url(r'^api/v1/auth/registration/', include('rest_auth.registration.urls')),
    url(r'^api/v1/auth/', include('rest_auth.urls')),
    url(r'^', TemplateView.as_view(template_name="index.html"))
]
