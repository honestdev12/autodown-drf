from rest_framework import serializers

from rest_auth.models import TokenModel
from rest_auth.registration.serializers import RegisterSerializer
from vendor.serializers import UserSerializer


class TokenSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    token = serializers.CharField(source='key', read_only=True)
    key = serializers.CharField(write_only=True)

    class Meta:
        model = TokenModel
        fields = ('key', 'user', 'token', )


class CustomRegisterSerializer(RegisterSerializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
        }

