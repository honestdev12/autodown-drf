from django.contrib.auth.models import User
from rest_framework import serializers

import vendor.models as models
from admin.config import config


class UserDetailSerailizer(serializers.ModelSerializer):
    phone_number = serializers.RegexField(r'^\+?\d{10}$', required=False)
    post_code = serializers.RegexField(r'^[a-zA-Z1-9]{3-10}$', required=False)

    class Meta:
        model = models.UserDetail
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    roles = serializers.SerializerMethodField('get_user_roles', read_only=True)
    detail = UserDetailSerailizer(allow_null=True, read_only=True)
    unpaid_inv = serializers.SerializerMethodField('get_unpaid_invoice')
    
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'detail', 'id', 'roles', 'username', 'unpaid_inv', 'is_active', )

    @staticmethod
    def get_user_roles(obj):
        roles = ['USER']
        if obj.is_superuser:
            roles.append('ADMIN')
        return roles

    @staticmethod
    def get_unpaid_invoice(obj):
        return models.Invoice.objects.filter(user=obj, paid=False).count()

    def create(self, validated_data):
        request = self.context.get('request', {})
        detail = request.data.get('detail')
        user = User.objects.create(**validated_data)
        models.UserDetail.objects.create(user=user, **detail)
        return user

    def update(self, instance, validated_data):
        request = self.context.get('request', {})
        detail = request.data.get('detail')
        user = super(UserSerializer, self).update(instance, validated_data)
        updated = models.UserDetail.objects.filter(user=user).update(**detail)
        if not updated:
            models.UserDetail.objects.create(user=user, **detail)
        return user


class DropBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DropBox
        fields = ('id', 'token', 'name', 'user')

    def create(self, validated_data):
        request = self.context.get('request', {})
        dropbox = models.DropBox.objects.create(user=request.user, **validated_data)
        return dropbox


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Schedule
        fields = ('months', 'weeks', 'days', 'hours', 'id', )


class VendorSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VendorSite
        fields = '__all__'


class SpiderLogsSerializer(serializers.ModelSerializer):
    vendor_site_name = serializers.CharField(source='spider_job.vendor_site.name', read_only=True)
    download_time = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = models.SpiderLogs
        fields = '__all__'

    @staticmethod
    def get_download_time(obj):
        return obj.created.strftime('%Y-%m-%d')


class SpiderJobSerializer(serializers.ModelSerializer):
    vendor_site = VendorSiteSerializer(read_only=True)
    user = UserSerializer(read_only=True)
    logs = SpiderLogsSerializer(read_only=True)

    class Meta:
        model = models.SpiderJob
        fields = '__all__'

    def create(self, validated_data):
        request = self.context.get('request', {})
        user = request.user
        vendorsite_name = request._request.resolver_match.kwargs.get('vendorsite_name')
        vendorsite = models.VendorSite.objects.get(name=vendorsite_name)

        if not vendorsite:
            raise serializers.ValidationError('Can not find the vendoersite')

        spider_job = models.SpiderJob.objects.create(vendor_site=vendorsite, user=user, **validated_data)
        return spider_job

    def update(self, instance, validated_data):
        request = self.context.get('request', {})
        dropbox_id = request.data.get('dropbox')
        if instance.dropbox != dropbox_id and not dropbox_id:
            dropbox = models.DropBox.objects.get(id=dropbox_id)
            instance.dropbox = dropbox
            instance.save()

        return super(SpiderJobSerializer, self).update(instance, validated_data)


class InvoiceSpiderLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.InvoiceSpiderLog
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    total_count = serializers.SerializerMethodField()

    class Meta:
        model = models.Invoice
        exclude = ('user', )

    @staticmethod
    def create_related(invoice, spider_logs):
        for spider_log in spider_logs:
            try:
                models.InvoiceSpiderLog.objects.create(invoice=invoice, spider_log=spider_log)
            except:
                continue

    @staticmethod
    def get_total_count(obj):
        return models.InvoiceSpiderLog.objects.filter(invoice=obj).count()

    def create(self, validated_data):
        request = self.context.get('request', {})
        spider_logs = models.SpiderLogs.objects.filter(
            created__range=[validated_data['start_date'].strftime('%Y-%m-%d'), validated_data['end_date'].strftime('%Y-%m-%d')],
            spider_job__user=request.user
        )
        if not spider_logs:
            raise serializers.ValidationError(
                'There are no downladed files from {} to {}'.format(
                    validated_data['start_date'].strftime('%Y-%m-%d'), 
                    validated_data['end_date'].strftime('%Y-%m-%d')
                )
            )

        validated_data['rate'] = float(config.get('rate'))

        validated_data['price_amount'] = float(config.get('rate')) * len(spider_logs)

        invoice = models.Invoice.objects.create(user=request.user, **validated_data)

        self.create_related(invoice, spider_logs)

        return invoice

    def update(self, instance, validated_data):
        start_date = validated_data['start_date']
        end_date = validated_data['end_date']
        request = self.context.get('request', {})

        if start_date == instance.start_date and end_date == instance.end_date:
            return super(InvoiceSerializer, self).update(instance, validated_data)

        # delete all asigned to this invoice
        models.InvoiceSpiderLog.objects.filter(invoice=instance).delete()

        spider_logs = models.SpiderLogs.objects.filter(
            created__range=[validated_data['start_date'].strftime('%Y-%m-%d'), validated_data['end_date'].strftime('%Y-%m-%d')],
            spider_job__user=request.user
        )

        invoice = super(InvoiceSerializer, self).update(instance, validated_data)

        self.create_related(invoice, spider_logs)

        return invoice

