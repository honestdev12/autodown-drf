# Generated by Django 2.0.1 on 2018-09-29 01:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DropBox',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('token', models.TextField()),
                ('directory', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('hours', models.IntegerField(default=0)),
                ('days', models.IntegerField(default=1)),
                ('weeks', models.IntegerField(default=0)),
                ('months', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='SpiderJob',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('username', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('status', models.BooleanField(default=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SpiderLogs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('bill_date', models.DateTimeField()),
                ('vendor_name', models.CharField(max_length=255)),
                ('file_link', models.CharField(max_length=255)),
                ('invoice_id', models.CharField(max_length=255)),
                ('account_number', models.CharField(max_length=255)),
                ('spider_job', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='vendor.SpiderJob')),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='VendorSite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=255, unique=True)),
                ('spider_name', models.CharField(max_length=255, unique=True)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.AddField(
            model_name='spiderjob',
            name='vendor_site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vendor.VendorSite'),
        ),
        migrations.AddField(
            model_name='schedule',
            name='spider_job',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='vendor.SpiderJob'),
        ),
        migrations.AddField(
            model_name='dropbox',
            name='spider_job',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='vendor.SpiderJob'),
        ),
        migrations.AlterUniqueTogether(
            name='spiderlogs',
            unique_together={('spider_job', 'invoice_id', 'file_link')},
        ),
        migrations.AlterUniqueTogether(
            name='spiderjob',
            unique_together={('vendor_site', 'username')},
        ),
    ]
