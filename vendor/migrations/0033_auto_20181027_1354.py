# Generated by Django 2.0.1 on 2018-10-27 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0032_remove_spiderjob_schedule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='bill_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='end_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='start_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='spiderjob',
            name='from_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='spiderlogs',
            name='bill_date',
            field=models.DateField(null=True),
        ),
    ]
