from rest_framework import permissions, viewsets, status
from rest_framework.parsers import FileUploadParser, MultiPartParser

from vendor import models
from vendor import serializers

from rest_framework.renderers import StaticHTMLRenderer
from rest_framework.views import APIView
from datetime import datetime, timedelta, time
from rest_framework.response import Response
import dropbox
from admin.config import config
import json
from django.db.models import Q

from spiders.spiders.spiders.businesscomcast import BusinessComcastSpider
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

DATES_BY_PERIOD = {
    'week': 7,
    'month': 30
}


class VendorSiteViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.VendorSiteSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def get_queryset(self):
        if self.kwargs.get('all', '') == 'my':
            if models.DropBox.objects.filter(user=self.request.user).count() != 0:
                return models.VendorSite.objects.filter(Q(spiderjob__user=self.request.user) | Q(name='Dropbox'))
            else:
                return models.VendorSite.objects.filter(spiderjob__user=self.request.user)
        else:
            return models.VendorSite.objects.all()


class SpiderLogsViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SpiderLogsSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        try:
            search = json.loads(self.request.query_params.get('search'))
        except:
            search = {}
        filter_options = {
            'spider_job__user': self.request.user
        }
        for key in search:
            if search[key]:
                filter_options['{}__contains'.format(key)] = search[key]
        vendor_name = self.kwargs.get('vendor_name')
        if vendor_name:
            filter_options['spider_job__vendor_site__name'] = vendor_name

        return models.SpiderLogs.objects.filter(**filter_options)


class SpiderJobViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SpiderJobSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        vendorsite_name = self.kwargs.get('vendorsite_name')
        order = self.request.query_params.get('sort')
        if not order:
            order = 'created'
        return models.SpiderJob.objects.filter(user=self.request.user, vendor_site__name=vendorsite_name).order_by(order)


class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = models.Schedule.objects.all()
    serializer_class = serializers.ScheduleSerializer
    permission_classes = (permissions.IsAuthenticated,)


class DropBoxViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.DropBoxSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        order = self.request.query_params.get('sort')
        if not order:
            order = 'created'
        return models.DropBox.objects.filter(user=self.request.user).order_by(order)


class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.InvoiceSerializer
    permissions = (permissions.IsAuthenticated, )

    def get_queryset(self):
        order = self.request.query_params.get('sort')
        if not order:
            order = 'created'

        paid = self.kwargs.get('paid')

        if paid and paid in ['paid', 'unpaid']:
            return models.Invoice.objects.filter(
                user=self.request.user, paid=(paid=='paid'), spiderlog_set__isnull=False
            ).distinct().order_by(order)
        else:
            return models.Invoice.objects.filter(
                user=self.request.user,
                spiderlog_set__isnull=False
            ).distinct().order_by(order)


def daterange(d1, d2):
    return (d1 + timedelta(days=i) for i in range((d2 - d1).days + 1))


class ChartView(APIView):

    @staticmethod
    def get(request):
        vendor_id = request.query_params.get('vendor_id')
        period = request.query_params.get('period', 'week')
        from_date = request.query_params.get('from_date')
        end_date = request.query_params.get('end_date')

        from_date = datetime.strptime(from_date, '%Y-%m-%d') if from_date else None
        end_date = datetime.strptime(end_date, '%Y-%m-%d') if end_date else None

        if vendor_id != '-1':
            spiders = models.SpiderJob.objects.filter(vendor_site_id=vendor_id, user=request.user)
        else:
            spiders = models.SpiderJob.objects.filter(user=request.user)

        labels = []
        data = []

        if not all([from_date, end_date]):
            end_date = datetime.today()
            from_date = end_date - timedelta(days=DATES_BY_PERIOD.get(period))

        if not spiders:
            return Response({'error': 'Can not find the spiders'}, status=status.HTTP_400_BAD_REQUEST)
        datum = {
            'data': [],
            'label': spiders[0].vendor_site.name if vendor_id != '-1' else 'All'
        }

        for i in daterange(from_date, end_date):
            date_label = i.strftime('%Y-%m-%d')
            labels.append(date_label)
            j_max = datetime.combine(i - timedelta(days=1), time.max)
            i_max = datetime.combine(i, time.max)
            val = models.SpiderLogs.objects.filter(
                spider_job__in=spiders,
                created__range=(j_max, i_max)
            ).distinct().count()
            datum['data'].append(val)
        data.append(datum)

        return Response({
            'labels': labels,
            'data': data
        }, status=status.HTTP_200_OK)


class UploadView(APIView):
    parser_classes = (MultiPartParser, FileUploadParser, )

    def post(self, request, spider_job_id, format=None):
        dbx = dropbox.Dropbox(config.get('dropbox_token'))
        spider_job = models.SpiderLogs.objects.get(id=spider_job_id)
        if not spider_job_id:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        for key, file in request.FILES.items():
            filename = file.name
            res = dbx.files_upload(file.read(), '/{}'.format(filename), mute=True)
            file_link = dbx.sharing_create_shared_link_with_settings(res.path_display)
            models.SpiderLogs.objects.create(spider_job=spider_job, file_name=filename, file_link=file_link)
        return Response(status=status.HTTP_204_NO_CONTENT)


class SpiderTestView(APIView):

    def get(self, request):
        runner = CrawlerRunner()
        configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
        d = runner.crawl(
            BusinessComcastSpider, **{
                'user_name':'ap@res1.net',
                'password':'Guilford8975',
                'spider_job_id':7,
                'full_name': 'First Last',
                'dropbox_token':'OEdzG1j9VTAAAAAAAAAAKg1KHJL14nr5TjieSDLMNM0NqjcvCG3h7W_NFfATlT7E',
                'from_date':'2018-01-01',
                'user_id': 1
            }
        )
        d.addBoth(lambda  _:reactor.stop())
        reactor.run(installSignalHandlers=False)

        return Response({'status': 'ok'}, status.HTTP_200_OK)


class IndexView(APIView):
    renderer_classes = [StaticHTMLRenderer]
    template_name = 'index.html'

