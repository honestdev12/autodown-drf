from django.db import models
from django.contrib.auth.models import User


class UserDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='detail')
    avatar = models.CharField(max_length=255, null=True, blank=True)

    linkedin = models.CharField(max_length=255, null=True, blank=True)
    facebook = models.CharField(max_length=255, null=True, blank=True)
    twitter = models.CharField(max_length=255, null=True, blank=True)
    instagram = models.CharField(max_length=255, null=True, blank=True)

    address = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    state = models.CharField(max_length=255, null=True, blank=True)
    post_code = models.CharField(max_length=10, null=True, blank=True)

    phone_number = models.CharField(max_length=12, null=True, blank=True)
    company_name = models.CharField(max_length=255, null=True, blank=True)
    occupation = models.CharField(max_length=255, null=True, blank=True)


class VendorSite(models.Model):
    name = models.CharField(max_length=255)
    spider_name = models.CharField(max_length=255, unique=True, null=True)
    type = models.CharField(
        max_length=10,
        choices=(
            ('saas', 'saas'),
            ('website', 'website')
        ),
        default='website'
    )
    brand_img = models.CharField(max_length=255, null=False, default='', blank=True)

    class Meta:
        ordering = ('name', )


class DropBox(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    token = models.TextField(unique=True)
    name = models.CharField(max_length=255, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ('created', )


class Schedule(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    hours = models.IntegerField(default=0)
    days = models.IntegerField(default=1)
    weeks = models.IntegerField(default=0)
    months = models.IntegerField(default=0)

    class Meta:
        ordering = ('created', )


class SpiderJob(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    dropbox = models.ForeignKey(DropBox, on_delete=models.CASCADE, null=True)
    vendor_site = models.OneToOneField(VendorSite, on_delete=models.CASCADE)

    username = models.CharField(max_length=255, null=True, blank=True)
    password = models.CharField(max_length=255, null=True, blank=True)
    status = models.BooleanField(default=True)
    from_date = models.DateField(null=True, blank=True)


class SpiderLogs(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    spider_job = models.ForeignKey(SpiderJob, on_delete=models.CASCADE)
    bill_date = models.DateField(null=True)
    invoice_id = models.CharField(max_length=255, null=True)
    account_number = models.CharField(max_length=255, null=True)
    
    file_link = models.CharField(max_length=255)
    file_link_user = models.CharField(max_length=255, null=True, default='')
    file_name = models.CharField(max_length=255, null=True)

    class Meta:
        ordering = ('-created', )
        unique_together = ('spider_job', 'invoice_id', 'file_name',)


class Invoice(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField()
    end_date = models.DateField()
    bill_date = models.DateField(null=True, blank=True)

    payment_type = models.CharField(
        max_length=20,
        choices=(
            ('PayPal', 'PayPal'),
            ('Stripe', 'Stripe')
        ),
        default='PayPal'
    )

    paid = models.BooleanField(default=False)
    payment_id = models.CharField(max_length=255, null=True, blank=True)
    price_amount= models.DecimalField(max_digits=10, decimal_places=2, default=0.0, null=True, blank=True)
    rate= models.DecimalField(max_digits=10, decimal_places=2, default=0.0, null=True, blank=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created', )


class InvoiceSpiderLog(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='spiderlog_set')
    spider_log = models.OneToOneField(SpiderLogs, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-invoice',)

