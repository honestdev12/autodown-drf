from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter


from vendor import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'(?P<all>(my)?)/?vendor_site', views.VendorSiteViewSet, base_name='vendor_site')
router.register(r'(?P<vendorsite_name>[ \w]+)/spider_job', viewset=views.SpiderJobViewSet, base_name='spider_job')
router.register(r'(?P<vendor_name>[ \w]*)/?spider_logs', views.SpiderLogsViewSet, base_name="spider_logs")
router.register(r'schedule', views.ScheduleViewSet)
router.register(r'dropbox', views.DropBoxViewSet, base_name='dropbox')
router.register(r'(?P<paid>[\w]*)/?invoice', views.InvoiceViewSet, base_name='invoice')


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^chart_data', views.ChartView.as_view()),
    url(r'^test', views.SpiderTestView.as_view()),
    url(r'^(?P<spider_job_id>\d+)/upload', views.UploadView.as_view())
]
