import scrapy
from urllib import urlencode
import time


class PepcoSpider(scrapy.Spider):
    name = "pepco"
    items = []
    closed_spider = False
    logs = []

    def start_requests(self):
        with open("scrapy.log","r") as fs:
            self.logs = [i.strip() for i in fs.readlines()]
            fs.close()

        urls = [
            'https://webapps2.pepco.com/dashboard/pepco/Default.aspx'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.enter_username)

    def enter_username(self, response):
        return scrapy.FormRequest.from_response(response=response, formname='aspnetForm', formdata={
            'ctl00$ctnMainContent$txtUserName': 'EquityMgmt2LLC'
        }, callback=self.enter_password)

    def enter_password(self, response):
        return scrapy.Request(
            method='POST',
            url='https://webapps.pepcoholdings.com/auth/loginIDM.html',
            body=urlencode({
                'UserID': 'EquityMgmt2LLC',
                'Password': 'equitymgmt2'
            }),
            callback=self.after_login,
            dont_filter=True
        )

    def after_login(self, response):
        return scrapy.FormRequest.from_response(response=response, formname='aspnetForm', formdata={
            'ctl01$ctnMainContent$ScriptManager1': 'ctl01$ctnMainContent$udpPanel|ctl01$ctnMainContent$rptAccounts$ctl01$btnSelectAccount',
            'ctl01$ctnMainContent$rptAccounts$ctl01$btnSelectAccount': 'select'
        }, callback=self.before_view_plan)

    def before_view_plan(self, response):
        return scrapy.FormRequest.from_response(response=response, formname='aspnetForm', formdata={
            'ctl01$ctnMainContent$ScriptManager1': 'ctl01$ctnMainContent$udpPanel|ctl01$ctnMainContent$rptAccounts$ctl01$btnSelectAccount',
            'ctl01$ctnMainContent$rptAccounts$ctl01$btnSelectAccount': 'select'
        }, callback=self.view_plan)

    def view_plan(self, response):
        url = 'https://webapps2.pepco.com/dashboard/pepco/ebill.aspx'
        return scrapy.Request(url=url, dont_filter=True, callback=self.before_bills)

    def before_bills(self, response):
        url = response.xpath('//script[contains(text(), "window.location")]').re_first('window.location=\'(.*?)\';}')
        if url:
            return scrapy.Request(url=url, dont_filter=True, callback=self.view_bills)
        else:
            print('No urls')

    def view_bills(self, response):
        url = 'https://webapps2.pepco.com/ebill/pepco/phiweb/consumer/documents/menu.action?clearsessionhelper=true&dojo.preventCache={}'
        now_mili = int(round(time.time() * 1000))
        return scrapy.Request(url.format(now_mili), dont_filter=True, callback=self.parse_bills)

    def parse_bills(self, response):
        trs = response.xpath('//tr[contains(@class, "app_list_row")]')
        idx = response.meta.get('idx', 0)

        for tr in trs:
            idx += 1
            url = tr.xpath('.//a/@href').extract_first()
            account_number = tr.xpath('./td[position()=2]/text()').extract_first()
            bill_date = self.convert_date(tr.xpath('./td[position()=1]/a/text()').extract_first())
            bill_from_date = self.convert_date(tr.xpath('./td[position()=3]/text()').extract_first())
            bill_to_date = self.convert_date(tr.xpath('./td[position()=4]/text()').extract_first())
            pay_by = self.convert_date(tr.xpath('./td[position()=5]/text()').extract_first())
            billed_amount = tr.xpath('./td[position()=6]/text()').extract_first()

            if 'pdf_{}{}.pdf'.format(account_number, bill_date) in self.logs:
                continue

            item = {
                'AccountNumber': account_number,
                'BillDate': bill_date,
                'Bill From Date': bill_from_date,
                'Bill To Date': bill_to_date,
                'Pay By': pay_by,
                'Billed Amount': billed_amount,
                'ranking': idx
            }
            if url:
                yield scrapy.Request(url, callback=self.before_download_bill, dont_filter=True, meta={'item': item})

        navigation_link = response.xpath('//div[@id="delete_form_"]/@href').extract_first()
        if navigation_link:
            yield scrapy.Request(navigation_link, callback=self.parse_next_page, dont_filter=True, meta={'idx': idx})

    def parse_next_page(self, response):
        next_page_link = response.xpath(
            '//table[@class="app_list_pagenav"]//td//a[contains(text(), "Next")]/@href'
        ).extract_first()
        if next_page_link:
            now_mili = int(round(time.time() * 1000))
            return scrapy.Request(url='{}&dojo.preventCache={}'.format(next_page_link, now_mili), callback=self.parse_bills, dont_filter=True, meta=response.meta)

    def before_download_bill(self, response):
        url = response.xpath('//*[@id="document_div" and contains(@href, "https")]/@href').extract_first()
        if url:
            return scrapy.Request(
                url='{}&dojo.preventCache={}'.format(url, int(round(time.time() * 1000))),
                callback=self.download_page,
                dont_filter=True,
                meta=response.meta
            )

    def convert_date(self, dd):
        if not dd:
            return None
        dd = dd.replace('\n', '').split('/')
        if len(dd) == 3:
            return dd[2] + dd[0] + dd[1]

    def download_page(self, response):
        item = response.meta.get('item')
        pdf_url = response.xpath('//iframe/@src').extract_first()
        if pdf_url:
            item['file_url'] = pdf_url
            name = 'pdf_{}{}.pdf'.format(item['AccountNumber'], item['BillDate'])
            self.write_log(name)
            item['name'] = name
            return item

    def write_log(self, name):
        with open("scrapy.log", "a") as fs:
            fs.write(name + '\n')
            fs.close()
        self.logs.append(name)