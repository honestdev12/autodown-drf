from __future__ import division, absolute_import, unicode_literals
from scrapy import Request, Spider, FormRequest
import w3lib.html
import re
import json
import traceback
from urllib.parse import urlencode
from datetime import datetime
from dropbox.files import WriteMode
from admin.config import config
import dropbox
from vendor.models import SpiderLogs


class BusinessComcastSpider(Spider):
    name = "businesscomcast"
    start_urls = [
        'https://business.comcast.com'
    ]

    BILL_URL = 'https://business.comcast.com/myaccount/api/Billing/GetBillDetails'
    SWITCH_ACCOUNT_URL = 'https://business.comcast.com/myaccount/api/account/switchaccount'
    DOWNLOAD_URL = 'https://business.comcast.com/myaccount/api/Billing/GetSMBStatement'
    passed_accounts = []

    def __init__(
            self,
            user_name,
            password,
            spider_job_id,
            dropbox_token,
            from_date,
            user_id,
            full_name,
            *args, **kwargs
    ):
        super(BusinessComcastSpider, self).__init__(*args, **kwargs)
        self.user_name = user_name if user_name else 'ap@res1.net'
        self.password = password if password else 'Guilford8975'
        self.dropbox_token = dropbox_token
        self.from_date = datetime.strptime(from_date, '%Y-%m-%d') if from_date else datetime.now()
        self.spider_job_id = spider_job_id
        self.full_name = full_name
        self.user_id = user_id

    def parse(self, response):
        return Request(
            'https://business.comcast.com/myaccount/',
            callback=self.login,
            dont_filter=True
        )

    def login(self, response):
        content = w3lib.html.remove_tags_with_content(response.text, which_ones=('noscript', ))
        res = response.replace(body=bytes(content, 'utf-8'))
        return FormRequest.from_response(res, formid='sign-in-form', formdata={
            'user': self.user_name,
            'passwd': self.password
        }, callback=self.after_login)

    def after_login(self, response):
        return Request(
            'https://business.comcast.com/myaccount/Secure/MyAccount/Bills/BillLanding/',
            callback=self.bill_page,
            dont_filter=True,
            meta=response.meta
        )

    def bill_page(self, response):
        accounts = response.meta.get('accounts')
        if not accounts:
            prefix_api = re.search(r"customerContextApiPublicUri: '(.*?)'", response.text)
            prefix_api = prefix_api.group(1) if prefix_api else None
            user_id = re.search(r"userContextId : '(.*?)'", response.text)
            user_id = user_id.group(1) if user_id else None
            link = '{prefix_api}UserContext/{user_id}/Accounts'.format(prefix_api=prefix_api, user_id=user_id)
            token = re.search(r"token: '(.*?)'", response.text)
            token = token.group(1) if token else None
            if all([
                prefix_api,
                user_id,
                token
            ]):
                return Request(
                    link,
                    callback=self.get_accounts,
                    dont_filter=True,
                    headers={
                        'Accept': 'application/json, text/plain, */*',
                        'Authorization': 'Bearer {}'.format(token),
                    }
                )
        else:
            for key in accounts:
                if key not in self.passed_accounts:
                    return Request(
                        url=self.SWITCH_ACCOUNT_URL,
                        method='POST',
                        dont_filter=True,
                        body=json.dumps({"AccountNumber": key, "PageName": "", "PhoneNumber": ""}),
                        callback=self.switch_accounts,
                        meta=response.meta,
                        headers={
                            'content-type': 'application/json',
                            'accept': 'application/json, text/plain, */*'
                        }
                    )

    def get_accounts(self, response):
        try:
            data = json.loads(response.text)
            accounts = data.get('data', {}).get('accounts', [])
            accounts_dict = {}
            cur_account_id = ''
            for account in accounts:
                account_id = account.get('accountId')
                accounts_dict[account_id] = account
                if account.get('isCurrentAccount'):
                    self.passed_accounts.append(account_id)
                    cur_account_id = account_id
        except:
            self.logger.error('Error in parsing accounts json: {}'.format(traceback.format_exc()))
        else:
            return Request(
                url=self.BILL_URL,
                method='POST',
                callback=self.bill_detail,
                dont_filter=True,
                meta={
                    'accounts': accounts_dict,
                    'cur_account_id': cur_account_id
                }
            )

    def switch_accounts(self, response):
        response.meta['accounts'] = None
        return self.after_login(response)

    def bill_detail(self, response):
        meta = response.meta.copy()
        try:
            data = json.loads(response.text)
            bill_id = data.get('BillSummary', {}).get('BillStatementId')
            bill_start = data.get('BillSummary', {}).get('BillStartDate')
            bill_end = data.get('BillSummary', {}).get('BillEndDate')
            bill_due = data.get('BillStatus', {}).get('BillDueDate')

            if SpiderLogs.objects.filter(invoice_id=bill_id, spider_job_id=self.spider_job_id).count() != 0 \
                    or (bool(bill_due) and datetime.strptime(bill_due, '%b %d, %Y') < self.from_date):
                return self.after_login(response)

            bill_date = datetime.strptime(bill_due, '%b %d, %Y').strftime('%Y-%m-%d') if bill_due else None

            meta['bill_start'] = bill_start
            meta['bill_end'] = bill_end
            meta['bill_id'] = bill_id
            meta['bill_due'] = bill_date
        except:
            self.logger.error('Error in parsing the json of bill detail: {}'.format(traceback.format_exc()))
            return self.after_login(response)
        else:
            return Request(
                url=self.DOWNLOAD_URL,
                method='POST',
                body=urlencode({
                    'BillId': bill_id
                }),
                headers={
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'content-type': 'application/x-www-form-urlencoded'
                },
                callback=self.download_page,
                meta=meta,
                dont_filter=True
            )

    def download_page(self, response):
        meta=response.meta.copy()
        accounts = meta.get('accounts', {})
        cur_account_id= meta.get('cur_account_id', '')
        bill_start = self.date_to_string(meta.get('bill_start'))
        bill_end = self.date_to_string(meta.get('bill_end'))

        raw_pdf = response.body

        fname = accounts.get(cur_account_id, {}).get('displayTitle', '')
        file_name = '{}_({}-{}).pdf'.format(fname, bill_start, bill_end)

        file_link = self.upload_to_dropbox(file_name, raw_pdf, config.get('dropbox_token'), False)
        # file_link_user = self.upload_to_dropbox(file_name, raw_pdf, self.dropbox_token, True)

        SpiderLogs.objects.create(
            spider_job_id=self.spider_job_id,
            file_name=file_name,
            file_link=file_link,
            file_link_user=file_link,
            account_number=cur_account_id,
            bill_date=meta.get('bill_due'),
            invoice_id=meta.get('bill_id')
        )

        if len(self.passed_accounts) < len(accounts):
            return self.after_login(response)

    def date_to_string(self, d):
        d = d.split('/')
        return ''.join([i.zfill(2) for i in d])

    def upload_to_dropbox(self, filename, content, dropbox_token, user):
        dbx = dropbox.Dropbox(dropbox_token)
        upload_path = '/{} - {} - Comcast Business/{}'.format(
            self.user_id, self.full_name, filename
        ) if not user else '/Comcast Business/{}'.format(filename)
        res = dbx.files_upload(
            content,
            upload_path,
            mode=WriteMode('overwrite')
        )
        try:
            file_link = dbx.sharing_create_shared_link_with_settings(res.path_display).url
        except:
            file_link = dbx.sharing_list_shared_links(upload_path)
        return file_link

