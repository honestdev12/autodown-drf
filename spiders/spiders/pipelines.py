# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import dropbox

class SpidersPipeline(object):
    def process_item(self, item, spider):
        return item

    def upload_to_dropbox(self, filename, content, dropbox_token, user):
        dbx = dropbox.Dropbox(dropbox_token)
        upload_path = '/{} - {} - Comcast Business/{}'.format(
            self.user_id, self.full_name, filename
        ) if not user else '/Comcast Business/{}'.format(filename)
        res = dbx.files_upload(
            content,
            upload_path,
            mode=WriteMode('overwrite')
        )
        file_link = dbx.sharing_create_shared_link_with_settings(res.path_display)
        return file_link